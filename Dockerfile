FROM       busybox
MAINTAINER Bernd Fischer <bfischer@mindapproach.de>

RUN mkdir /var/jenkins_home
RUN chown default /var/jenkins_home
RUN chmod 750 /var/jenkins_home
